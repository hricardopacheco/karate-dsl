Feature: CardTypes Data

Background:
	* url randomUrl
	* configure headers = read('classpath:random/randomHeader.json')

@responseSuccess
Scenario: Card types
	Given path 'Card/Types'
	When method GET
	* def types = response
	* print types

@responseFailed
Scenario: Use incorrect HTTP Mehtod
Given path 'Card/Types'
	When method POST
	Then print response

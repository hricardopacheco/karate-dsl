Feature:Test all Scenarios of Random Credit Cards

Background:
	* url randomUrl
	* configure headers = read('classpath:random/randomHeader.json')
	* def Random = Java.type("utils.Random")
	* def jRandom = new Random()

Scenario: Success request and body
	* def rIndex = jRandom.randomIndex(5)
	* def cardType = call read('classpath:random/Cardtypes.feature@responseSuccess')
	* print cardType.types[rIndex]
	* def getCardType = cardType.types[rIndex]
	* def parameters = {type: "changeThis"}
	* set parameters.type = getCardType
	And print parameters
	Given path 'Card'
	* params parameters
	When method GET
	* print response
	Then status 200

Scenario: Validate body Response of Card
	Given path 'Card'
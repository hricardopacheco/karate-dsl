package random;

import com.intuit.karate.junit5.Karate;

class CardRunner {
    
    @Karate.Test
    Karate testCards() {
        return Karate.run("Card").relativeTo(getClass());
    }    

}

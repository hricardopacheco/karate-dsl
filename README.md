# Karate DSL - hpacheco

Is an execercice to explore Karate Framework and to learn about how to used it, and make libraries to use in Automation Test.

In this project, i used some open API's and RESTFull to validate and make asserts

## Installation

You need Java JVM 
https://www.oracle.com/java/technologies/downloads/

Also Maven
https://maven.apache.org/install.html

to create a new project or init an arquitype use, plase change the artifact and de groupId with your info.

```bash
mvn archetype:generate -DarchetypeGroupId=com.intuit.karate -DarchetypeArtifactId=karate-archetype -DarchetypeVersion=1.1.0 -DgroupId=com.xperion -DartifactId=KarateInit
```

## Usage

Just send a basic Maven test, this will be send all the test that in the project be.

```bash
mvn test
```

Is a test that validate status code, body with api key, from: https://randommer.io/

```bash
mvn test -Dtest=CardRunner
```

If you want to run a specific scenario use
```
mvn test -Dtest=CardRunner -Dkarate.options="--tags @Success"
```
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
OPEN SOURCE.



## References
https://github.com/karatelabs/

https://stackoverflow.com/questions/50692722/is-it-possible-to-include-and-exclude-via-tags-on-the-same-karate-command-line/50693388#50693388